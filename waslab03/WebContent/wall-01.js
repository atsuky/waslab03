var baseURI = "http://localhost:8080/waslab03";
var tweetsURI = baseURI+"/tweets";

var req;
var tweetBlock = "	<div id='tweet_{0}' class='wallitem'>\n\
	<div class='likes'>\n\
	<span class='numlikes'>{1}</span><br /> <span\n\
	class='plt'>people like this</span><br /> <br />\n\
	<button onclick='{5}Handler(\"{0}\")'>{5}</button>\n\
	<br />\n\
	</div>\n\
	<div class='item'>\n\
	<h4>\n\
	<em>{2}</em> on {4}\n\
	</h4>\n\
	<p>{3}</p>\n\
	</div>\n\
	</div>\n";

String.prototype.format = function() {
	var args = arguments;
	return this.replace(/{(\d+)}/g, function(match, number) { 
		return typeof args[number] != 'undefined'
			? args[number]
		: match
		;
	});
};

function likeHandler(tweetID) {
	var target = 'tweet_' + tweetID;
	var uri = tweetsURI+ "/" + tweetID +"/likes";
	// e.g. to like tweet #6 we call http://localhost:8080/waslab03/tweets/6/like
	
	req = new XMLHttpRequest();
	req.open('POST', uri, /*async*/true);
	req.onreadystatechange = function() {
		if (req.readyState == 4 && req.status == 200) {
			document.getElementById(target).getElementsByClassName("numlikes")[0].innerHTML = req.responseText;
		}
	};
	req.send(/*no params*/null);
}

function deleteHandler(tweetID) {

	req = new XMLHttpRequest();
	req.open('DELETE', tweetsURI + "/" +  tweetID, /*async*/true);
	req.onreadystatechange = function() {
		if (req.readyState == 4 && req.status == 200) {
			deleted = document.getElementById("tweet_"+tweetID);
			localStorage.removeItem(tweetID);
			deleted.remove();
		}
	};
	token = localStorage.getItem(tweetID); 
	json_req = JSON.stringify({ token: token});
	req.send(json_req);
	
}

function getTweetHTML(tweet, action) {  // action :== "like" xor "delete"
	var dat = new Date(tweet.date);
	var dd = dat.toDateString()+" @ "+dat.toLocaleTimeString();
	return tweetBlock.format(tweet.id, tweet.likes, tweet.author, tweet.text, dd, action);
	
}

function getTweets() {
	req = new XMLHttpRequest(); 
	req.open("GET", tweetsURI, true); 
	req.onreadystatechange = function() {
		if (req.readyState == 4 && req.status == 200) {
			var tweet_list = req.responseText;
			
			obj = JSON.parse(tweet_list);
			
			for (var element in obj){
				test = localStorage.getItem(obj[element]['id']);
				tweet_html = "";
				if (test != null){
					tweet_html = getTweetHTML(obj[element], "delete");
				}
				else {
					tweet_html = getTweetHTML(obj[element], "like");
				}
				document.getElementById("tweet_list").innerHTML += tweet_html;
			}
		
		};
	};
	req.send(null); 
};


function tweetHandler() {
	var author = document.getElementById("tweet_author").value;
	var text = document.getElementById("tweet_text").value;
	/*
* TASK #3 -->
				*/
	
	req = new XMLHttpRequest();
	req.open('POST', tweetsURI, /*async*/true);
	req.onreadystatechange = function() {
		if (req.readyState == 4 && req.status == 200) {
			resp_json = JSON.parse(req.responseText);
			localStorage.setItem(resp_json['id'], resp_json['token']);
			tweet_html = getTweetHTML(resp_json, "delete");
			aux = document.getElementById("tweet_list").innerHTML;
			document.getElementById("tweet_list").innerHTML = tweet_html + aux;
		}
	};
	req.setRequestHeader("Content-Type", "application/json");
	json_req = JSON.stringify({ author: author, text: text });
	req.send(json_req);
	
	// clear form fields
	document.getElementById("tweet_author").value = "";
	document.getElementById("tweet_text").value = "";

};

//main
function main() {
	document.getElementById("tweet_submit").onclick = tweetHandler;
	getTweets();
};
